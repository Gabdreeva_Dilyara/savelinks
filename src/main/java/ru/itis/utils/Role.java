package ru.itis.utils;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         27.02.2018
 */
public enum Role {
    ADMIN, TEACHER, STUDENT
}
