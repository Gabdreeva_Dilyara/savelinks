package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.forms.FormTeacher;
import ru.itis.models.Teacher;
import ru.itis.models.User;
import ru.itis.repositories.TeacherRepository;
import ru.itis.repositories.UserRepository;
import ru.itis.utils.Role;

import java.util.Optional;
import java.util.Set;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 03.04.2018
 */

@Service
public class TeacherService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();


    @Transactional
    public void register(FormTeacher userForm) {
        User user = new User();
        Teacher teacher = new Teacher();

        user.setPassword(passwordEncoder.encode(userForm.getPassword()));
        user.setRole(Role.TEACHER);
        user.setLogin(userForm.getLogin());
        user.setTeacher(teacher);

        teacher.setName(userForm.getName());
        teacher.setLastName(userForm.getLastName());
        teacher.setUser(user);

        userRepository.save(user);
        teacherRepository.save(teacher);
    }

    public Teacher getTeacher(User user) throws Exception{
        Optional<Teacher> teacher = teacherRepository.findByUser(user);
        if (teacher.isPresent()) {
            return teacher.get();
        }
        else {
            throw new Exception("user not found");
        }
    }
}
