package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.forms.FormStudent;
import ru.itis.forms.FormTeacher;
import ru.itis.models.Student;
import ru.itis.models.Teacher;
import ru.itis.models.User;
import ru.itis.repositories.StudentRepository;
import ru.itis.repositories.TeacherRepository;
import ru.itis.repositories.UserRepository;
import ru.itis.utils.Role;

import java.util.Optional;
import java.util.Set;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 03.04.2018
 */

@Service
public class StudentService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StudentRepository studentRepository;

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();


    @Transactional
    public void register(FormStudent userForm) {
        User user = User.builder()
                .login(userForm.getLogin())
                .password(passwordEncoder.encode(userForm.getPassword()))
                .role(Role.STUDENT)
                .build();

        Student student = Student.builder()
                .lastName(userForm.getLastName())
                .name(userForm.getName())
                .build();
        user.setStudent(student);

        userRepository.save(user);
        student.setUser(user);
        studentRepository.save(student);
    }

    public Student getStudent(User user) throws Exception{
        Optional<Student> student = studentRepository.findByUser(user);
        if (student.isPresent()) {
            return student.get();
        }
        else {
            throw new Exception("user not found");
        }
    }
}
