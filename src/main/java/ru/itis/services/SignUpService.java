package ru.itis.services;

import org.springframework.stereotype.Service;
import ru.itis.models.User;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 26.02.2018
 */


public interface SignUpService {
    public void register(User userForm);
}
