package ru.itis.models;

import lombok.*;

import javax.persistence.*;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 03.04.2018
 */

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
@Table(name = "teachers")
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String lastName;

    private int jobExperience;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "teacher")
    private User user;
}
