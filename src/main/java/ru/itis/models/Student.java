package ru.itis.models;

import lombok.*;

import javax.persistence.*;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 03.04.2018
 */

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
@Table(name = "students")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String lastName;

    private int phoneNumber;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "student")
    private User user;

}
