package ru.itis.models;


import lombok.*;
import ru.itis.utils.Role;

import javax.persistence.*;
import java.util.Set;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 26.02.2018
 */

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String login;

    private String password;

    @Enumerated(EnumType.STRING)
    private Role role;

    @OneToOne(mappedBy = "user")
    private Student student;

    @OneToOne(mappedBy = "user")
    private Teacher teacher;

}
