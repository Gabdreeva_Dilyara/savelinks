package ru.itis.forms;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 03.04.2018
 */
//формочка
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FormStudent {

    private String login;
    private String password;
    private String name;
    private String lastName;
    private String phoneNumber;
}
