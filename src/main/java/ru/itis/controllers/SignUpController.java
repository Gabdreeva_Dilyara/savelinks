package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.itis.forms.FormStudent;
import ru.itis.forms.FormTeacher;
import ru.itis.models.User;
import ru.itis.services.AuthService;
import ru.itis.services.StudentService;
import ru.itis.services.TeacherService;
import javax.validation.Valid;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 26.02.2018
 */

@Controller
@RequestMapping("/sign-up")
public class SignUpController {

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private AuthService authService;

    @GetMapping(value = "/student")
    public String getPageLoginStudent(ModelMap model){
        return "signup-student";
    }

    @PostMapping(value = "/student")
    public String signUpStudent(@Valid FormStudent user, BindingResult result, Model model) {
        studentService.register(user);
        return "success-reg";
    }
    @GetMapping(value = "/teacher")
    public String getPageLoginTeacher(ModelMap model){
        return "signup-teacher";
    }

    @PostMapping(value = "/teacher")
    public String signUpTeacher(@Valid FormTeacher user, BindingResult result, ModelMap modelMap) {
        teacherService.register(user);
        return "success-reg";
    }
}
