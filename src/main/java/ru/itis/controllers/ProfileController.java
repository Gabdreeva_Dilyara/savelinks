package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import ru.itis.models.Student;
import ru.itis.models.Teacher;
import ru.itis.models.User;
import ru.itis.repositories.UserRepository;
import ru.itis.services.AuthService;
import ru.itis.services.AuthServiceImpl;
import ru.itis.services.StudentService;
import ru.itis.services.TeacherService;
import ru.itis.utils.Role;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 26.02.2018
 */

@Controller
public class ProfileController {

    @Autowired
    private AuthService authService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private TeacherService teacherService;

    @GetMapping("profile")
    public String getProfile(Authentication authentication, @ModelAttribute("model") ModelMap model) throws Exception {
        User user = authService.getUserByAuthentication(authentication);
        
        if (user.getRole().equals(Role.STUDENT)) {
            Student student = studentService.getStudent(user);
            model.addAttribute("user", student);
        }
        else if (user.getRole().equals(Role.TEACHER)){
            Teacher teacher = teacherService.getTeacher(user);
            model.addAttribute("user", teacher);
        }

        return "profile";
    }

    @GetMapping("/")
    public String root(Authentication authentication) {
        if (authentication != null) {
            User user = authService.getUserByAuthentication(authentication);
            if (user.getRole().equals(Role.STUDENT)) {
                return "redirect:/profile";
            }
            else if (user.getRole().equals(Role.TEACHER)){
                return "redirect:/profile";
            }
        }
        return "redirect:/sign-in";
    }
}
