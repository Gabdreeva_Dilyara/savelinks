package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import ru.itis.models.User;
import ru.itis.services.AuthService;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 26.02.2018
 */

@Controller
public class AuthController {

    @Autowired
    private AuthService authService;

    @GetMapping("/sign-in")
    public String signIn(@ModelAttribute("model")ModelMap model, Authentication authentication) {
        if (authentication != null) {
            return "redirect:/";
        }
        return "login";
    }
    @GetMapping("/logout")
    public String logout(HttpServletRequest request, Authentication authentication) {
        if (authentication != null) {
            request.getSession().invalidate();
        }
        return "redirect:/sign-in";
    }

}
