package ru.itis.config.secuirity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.itis.models.User;
import ru.itis.repositories.UserRepository;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 26.02.2018
 */

@Service
public class UserDetailsServiceImpl implements UserDetailsService {


    //менять здесь
    private UserRepository userRepository;

    @Autowired
    public UserDetailsServiceImpl(UserRepository usersRepository) {
        this.userRepository = usersRepository;
    }

    //менять здесь
    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userRepository.findByLogin(login).orElseThrow(()
                -> new IllegalArgumentException("Пользователь с таким логином  <" + login + ">"));
        return new UserDetailsImpl(user);
    }

}
