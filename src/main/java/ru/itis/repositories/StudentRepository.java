package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itis.models.Student;
import ru.itis.models.Teacher;
import ru.itis.models.User;

import java.util.Optional;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 26.02.2018
 */

@Repository
public interface StudentRepository extends JpaRepository<Student, Long>{
    Optional<Student> findByUser(User user);
}
